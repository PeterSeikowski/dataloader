import random
import torch
import numpy
import collections.abc
import multiprocessing
import itertools
import queue



def default_collate(batch):
    # Collate_fn that can be given to the DataLoader

    # Check if batch is a single value
    if not isinstance(batch, collections.abc.Sequence):
        return batch
    
    # If a sequence of tensors is given, return them stacked in the first dimension
    if isinstance(batch[0], torch.Tensor):
        return torch.stack(batch, 0)
    
    # If a sequence of sequences is given, unpac them recursevly
    elif isinstance(batch[0], collections.abc.Sequence):
        list_of_batch = zip(*batch)
        return [default_collate(samples) for samples in list_of_batch]
    
    # If there is no sequence given, return the batch
    else:
        return batch




def worker_fn(data, index_queue, shared_queue):
    # Function the workers will execute, loading items into the shared_queue according to index_queue
        
    # Constantly check if there are items in the index_queue
    while True:
        try:
            index = index_queue.get(timeout=0)
        except queue.Empty:
            continue

        # We use a None element to mark the end of the queue in order to end the process
        if index == None:
            break

        # If an index was found, put the according (index,item) tuple in the shared_queue
        shared_queue.put((index,data[index]))




class DataLoader():

    def __init__(
            self, 
            data, 
            batchsize, 
            num_workers=1, 
            collate_fn =default_collate,  
            shuffle= False,
            prefetch_size = 2,
    ):
        self.data = data
        self.batchsize = batchsize
        self.num_workers= num_workers
        self.collate_fn = collate_fn
        self.index = 0
        self.shuffle = shuffle

        # The Queue that is shared by the different workers, they will load items into it and and the get() method will retrieve from it
        self.shared_queue = multiprocessing.Queue()

        # The list of index queues that each worker has, to know which items to load into the shared queue
        self.index_queues = []

        # A dictionary to store items retrieved by the get() function from the shared_queue, in case the index is not the one in line
        self.cache_queue = {}
        
        self.workers = []
        self.worker_cycle = itertools.cycle(range(self.num_workers))

        # Number of batches that each worker will prefetch into the shared_queue (total numer of prefatched batches = num_workers* prefetch_size)
        self.prefetch_size = prefetch_size
        
        # Index that runs in the prefetch cycle to load the index_queues
        self.prefetch_index = 0

        
    

    def __next__(self):
        
        # Check if there is enough data left for a full batch
        if len(self.data) - (self.index) < self.batchsize:
            # Call delete function at the end of one itereation cycle
            self.__del__()
            raise StopIteration
        
        # Call the get() Method to feed a list of length batchsize into the collate function
        next = self.collate_fn([self.get() for _ in range(self.batchsize)])
        
        return next
    


    def __iter__(self):
        
        # Shuffle data if shuffle = True
        if self.shuffle == True:
            random.shuffle(self.data)

        # Reset all important class variables
        self.index=0
        self.prefetch_index=0
        self.cache_queue = {}
        self.index_queues = []
        self.workers = []
        self.shared_queue = multiprocessing.Queue()

        # Initializing the workers and their respected index_queues
        for _ in range(self.num_workers):
            index_queue = multiprocessing.Queue()
            worker = multiprocessing.Process(target= worker_fn, args=(self.data, index_queue, self.shared_queue))
            self.index_queues.append(index_queue)
            self.workers.append(worker)

            # daemon = true ensures that the process terminates as soon as the main process is finished
            worker.daemon = True
            
            worker.start()

        return self
    


    def get(self):
        self.prefetch_fn()
        
        # Check if desired index is in the cache_queue
        if self.index in self.cache_queue: 
                    item = self.cache_queue[self.index]
                    del self.cache_queue[self.index]

        else:
            # If the index was not in the cache_queue, keep loading items from the shared_queue until the correct index is found
            # while storing items with a different index in the cache_queue
            while True:
                try:
                    index, queue_item = self.shared_queue.get(timeout=0)

                    if self.index==index:
                        item = queue_item
                        break
                    else:
                        self.cache_queue[index] = queue_item

                except queue.Empty:
                    continue

        self.index += 1
        return item

            


    
    def prefetch_fn(self):
        # Loads indexes into the index_queues of the workers

        # Check if prefetch_index is within the length of the dataset and if prefetch_index is maximal prefetch_size* batchsize ahead of the actual index
        while (self.prefetch_index < len(self.data) and (self.prefetch_index < self.index + (self.prefetch_size * self.batchsize))):
            self.index_queues[next(self.worker_cycle)].put(self.prefetch_index)
            self.prefetch_index +=1
        
    
            
    def __del__(self):

        # Terminate the workers by passing None to the index_queues and close the queues
        try:
            for i, w in enumerate(self.workers):
                self.index_queues[i].put(None)

                # Wait for the worker process to terminate
                w.join(timeout=5.0)

            for i_queue in self.index_queues:
                i_queue.cancel_join_thread()
                i_queue.close()

            self.shared_queue.cancel_join_thread()
            self.shared_queue.close()

        finally:
            for w in self.workers:
                if w.is_alive():
                    w.terminate()
    
        





        
            









